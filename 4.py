#!/usr/bin/env python

# --- Day 4: Secure Container ---
#
# You arrive at the Venus fuel depot only to discover it's protected by a
# password. The Elves had written the password on a sticky note, but someone
# threw it out.
#
# However, they do remember a few key facts about the password:
#
#     It is a six-digit number.
#     The value is within the range given in your puzzle input.
#     Two adjacent digits are the same (like 22 in 122345).
#     Going from left to right, the digits never decrease; they only ever increase or stay the same (like 111123 or 135679).
#
# Other than the range rule, the following are true:
#
#     111111 meets these criteria (double 11, never decreases).
#     223450 does not meet these criteria (decreasing pair of digits 50).
#     123789 does not meet these criteria (no double).
#
# How many different passwords within the range given in your puzzle input meet
# these criteria?
#
# Your puzzle input is 402328-864247.
#
# --- Part Two ---
#
# An Elf just remembered one more important detail: the two adjacent matching
# digits are not part of a larger group of matching digits.
#
# Given this additional criterion, but still ignoring the range rule, the following are now true:
#
#     112233 meets these criteria because the digits never decrease and all repeated digits are exactly two digits long.
#     123444 no longer meets the criteria (the repeated 44 is part of a larger group of 444).
#     111122 meets the criteria (even though 1 is repeated more than twice, it still contains a double 22).
#
# How many different passwords within the range given in your puzzle input meet
# all of the criteria?
#
# Your puzzle input is still 402328-864247.

def has_required_length(p):
    return len(p) == 6


def has_same_adjacent_digits(p):
    adjacent_digit_counts = {}
    previous = p[0]
    for digit in p[1:]:
        if digit == previous:
            if digit in adjacent_digit_counts:
                adjacent_digit_counts[digit] += 1
            else:
                adjacent_digit_counts[digit] = 2
        else:
            previous = digit
    for k, v in adjacent_digit_counts.items():
        if v == 2:
            return True
    return False


def digits_never_decrease(p):
    previous = p[0]
    for digit in p[1:]:
        if digit >= previous:
            previous = digit
        else:
            return False
    return True


def valid_password(p):
    password_string = str(p)
    return has_required_length(password_string) and \
       has_same_adjacent_digits(password_string) and \
       digits_never_decrease(password_string)


def main():
    start = 402328
    end = 864247
    passwords = []
    for i in range(start, (end + 1)):
        if valid_password(i):
            passwords.append(i)
    return len(passwords)


print(main())
