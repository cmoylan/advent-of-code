class Interpreter:
    def __init__(self, program):
        self.program = program
        self.exited = False


    def run(self):
        pointer = 0
        while not self.exited:
            pointer = self._process(pointer)
        return self.program


    def _process(self, position):
        """Perform the computation and return the position of the next opcode"""
        instruction = str(self.program[position]).zfill(5)
        opcode = instruction[-2:]
        parameter_modes = instruction[0:-2]

        if opcode == '01':
            a, b = self._operands(position)
            self.program[self._result_location(position)] = a + b
            return position + 4
        elif opcode == '02':
            a, b = self._operands(position)
            self.program[self._result_location(position)] = a * b
            return position + 4
        elif opcode == '03':
            a = input('enter program input')
            # TODO: remove whitespace on user input
            self.program[self.program[position + 1]] = int(a)
            return position + 2
        elif opcode == '04':
            a = self._operand(position + 1, parameter_modes[2])
            print("output: ", a)
            return position + 2
        elif opcode == '05':
            a, b = self._operands(position)
            if a != 0:
                return b
            else:
                return position + 3
        elif opcode == '06':
            a, b = self._operands(position)
            if a == 0:
                return b
            else:
                return position + 3
        elif opcode == '07':
            a, b = self._operands(position)
            if a < b:
                self.program[self.program[position+3]] = 1
                #store 1 in 3
            else:
                self.program[self.program[position+3]] = 0
                #store 0 in 3
            return position + 4
        elif opcode == '08':
            a, b = self._operands(position)
            if a == b:
                self.program[self.program[position+3]] = 1
                #store 1 in 3
            else:
                self.program[self.program[position+3]] = 0
                #store 0 in 3
            return position + 4
        elif opcode == '99':
            self.exited = True
        else:
            raise ValueError('Unknown opcode', opcode)


    def _operands(self, position):
        instruction = str(self.program[position]).zfill(5)
        parameter_modes = instruction[0:-2]

        return [self._operand(position + 1, parameter_modes[2]),
                self._operand(position + 2, parameter_modes[1])]


    def _operand(self, position, mode):
        if mode == '0':
            location = self.program[position]
            return self.program[location]
        elif mode == '1':
            return self.program[position]
        else:
            raise ValueError('Unsupported parameter mode')


    def _result_location(self, position):
        return self.program[position + 3]


from copy import copy

class Computer:
    def __init__(self, program):
        self.program = program

    def run(self, noun, verb):
        program = copy(self.program)
        program[1] = noun
        program[2] = verb
        result = Interpreter(program).run()
        return result[0]
